//
//  projetViewController.m
//  projet
//
//  Created by etudiant on 13/01/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "projetViewController.h"

@implementation projetViewController

@synthesize showScore;
@synthesize playButton;
@synthesize rulesButton;
@synthesize backButton;
@synthesize rulesView;


//éléments de la vue

/*------ IMAGES -----*/
UIImageView *background;
UIImageView *upBg;
UIImageView *containShip;
UIImageView *containApple;
UIImageView *containApple2;
UIImageView *containApple3;
UIImageView *containBombe;
UIImageView *menu;
UIImage *up;
UIImage *ship;
UIImage *shipJump;
UIImage *shipFall;
UIImage *shipLeft;
UIImage *apple;
UIImage *bombe;
/*------ IMAGES -----*/

/*------ ACCEL ------*/
UIAccelerometer *accel;
/*------ ACCEL ------*/

/*------ MOVIE ------*/
MPMoviePlayerViewController *movie;
/*------ MOVIE ------*/

/*------ INTERFACE -----*/
UIButton *playButton;
UIButton *rulesButton;
UIButton *backButton;
/*------ INTERFACE -----*/

//var
int vitesse;
int score;
int randX;
float bgMovementUp;
float bgMovementDown;
BOOL upDown;
BOOL hitTestP;
BOOL hitTestB;
BOOL hitStatusP;
BOOL hitStatusB;
BOOL touchStatus;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---- ACTION ----*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (IBAction)clicButton:(id)sender {
	[NSTimer scheduledTimerWithTimeInterval:0.05f target:self selector:@selector(initGame) userInfo:nil repeats:NO];
}

- (IBAction)clicButton2:(id)sender {
	[NSTimer scheduledTimerWithTimeInterval:0.05f target:self selector:@selector(initRules) userInfo:nil repeats:NO];
}

- (IBAction)clicButton3:(id)sender {
	[NSTimer scheduledTimerWithTimeInterval:0.05f target:self selector:@selector(emptyRules) userInfo:nil repeats:NO];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---- ACTION ----*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	[self lancementFilm:@"movie" type:@"mp4"];
	
	//Acceleromètre
	accel = [UIAccelerometer sharedAccelerometer];
	accel.delegate = self;
	accel.updateInterval = 1.0f/60.0f;
    
    //score
    showScore.alpha =0;
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------- MOVIE -----------*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-(void) lancementFilm: (NSString *) film type:(NSString *) leType {
	NSBundle *bundle=[NSBundle mainBundle];
	NSString *moviePath=[bundle pathForResource:film ofType:leType];
	movie=[[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:moviePath]];	
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finFilm:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
	
	
	[self.view addSubview:movie.view];
    
    
    //---play movie---
    MPMoviePlayerController *player= [movie moviePlayer];
    [player play];
	
}


-(void) finFilm:(NSNotification *) maNotif
{	
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
	[movie.view removeFromSuperview];
	
	[movie autorelease];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------- MOVIE -----------*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------- RULES -----------*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//accède à l'écran de règles
- (void) initRules
{
    
    rulesView.alpha = 1;
	rulesButton.alpha = 0;
	backButton.alpha = 1;
    
	/*------ REQUEST -------*/
	NSString *url = @"http://www.jalehouse.com/rules.png";
	[rulesView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
	/*------ REQUEST -------*/

}


//vide l'écran de règles
- (void) emptyRules
{
	rulesView.alpha = 0;
	rulesButton.alpha = 1;
	backButton.alpha = 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------- RULES -----------*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------- GAME -----------*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Lance le jeu
- (void) initGame
{
	//erase
	menu.alpha = 0;
	playButton.alpha = 0;
	rulesButton.alpha = 0;
    showScore.alpha = 1;
	
	
	/*-------- VARs ----------*/
	upDown = TRUE;
	hitStatusP = TRUE;
	hitStatusB = TRUE;
	touchStatus = FALSE;
	
	/* IMAGE KIRBY + CONTAINER */
	ship=[UIImage imageNamed:@"ship.png"];
	shipJump=[UIImage imageNamed:@"ship3.png"];
	shipLeft=[UIImage imageNamed:@"ship4.png"];
	shipFall=[UIImage imageNamed:@"ship5.png"];
	containShip=[[UIImageView alloc] initWithImage:ship];
	containShip.frame=CGRectMake(125,385,containShip.frame.size.width, containShip.frame.size.height);	
	/* ------ KIRBY ----- */
	
	/* IMAGE UP + CONTAINER */
	up=[UIImage imageNamed:@"up.jpg"];
	upBg=[[UIImageView alloc] initWithImage:up];
	upBg.frame=CGRectMake(0,-960,upBg.frame.size.width,upBg.frame.size.height);	
	/* ------ UP ----- */
	
	/* IMAGE POMME + CONTAINER */
	apple=[UIImage imageNamed:@"apple.png"];
	containApple=[[UIImageView alloc] initWithImage:apple];	
	containApple.frame=CGRectMake(200,-200,containApple.frame.size.width, containApple.frame.size.height);	
	/* ------ POMME ----- */
	
	/* IMAGE POMME2 + CONTAINER2 */
	containApple2=[[UIImageView alloc] initWithImage:apple];
	containApple2.frame=CGRectMake(100,-200,containApple2.frame.size.width, containApple2.frame.size.height);	
	/* ------ POMME ----- */
	
	/* IMAGE POMME3 + CONTAINER3 */
	containApple3=[[UIImageView alloc] initWithImage:apple];
	containApple3.frame=CGRectMake(100,-200,containApple3.frame.size.width, containApple3.frame.size.height);	
	/* ------ POMME ----- */
	
	/* IMAGE BOMBE + CONTAINER */
	bombe=[UIImage imageNamed:@"bombe.png"];
	containBombe=[[UIImageView alloc] initWithImage:bombe];
	containBombe.frame=CGRectMake(225,-200,containBombe.frame.size.width, containBombe.frame.size.height);	
	/* ------ BOMBE ----- */

	
	/* ELEMENTS DE LA VUE */
	[self.view addSubview:containShip];
	[self.view addSubview:upBg];
	[self.view addSubview:containApple];
	[self.view addSubview:containApple2];
	[self.view addSubview:containApple3];
	[self.view addSubview:containBombe];
	/* ------ VUE ------ */
	
	/* TIMERs */
	
	/*------OBJETS-------*/
	[NSTimer scheduledTimerWithTimeInterval:0.02f target:self selector:@selector(behaviours) userInfo:nil repeats:YES];
	
	
	/*------ SCORE ------*/
	showScore.text=@"00";
	
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------- GAME -----------*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------- BASIC --------------*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event 
{
	UITouch *touch = [[event allTouches] anyObject];
	CGPoint currentPosition = [touch locationInView:self.view];
	containShip.frame=CGRectMake(currentPosition.x,containShip.frame.origin.y,containShip.frame.size.width, containShip.frame.size.height);
}

//démarre le saut au toucher
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event 
{
	if(touchStatus == FALSE)
	{
		[self jumpKirby];
		touchStatus = TRUE;
	}
}

//gestion de déplacement avec l'acceleromètre
- (void)accelerometer:(UIAccelerometer *)accel didAccelerate:(UIAcceleration *)aceler 
{
	if(aceler.x>0.2f)
	{
		containShip.frame=CGRectMake(containShip.frame.origin.x+3,containShip.frame.origin.y,containShip.frame.size.width, containShip.frame.size.height);
	}
	else if(aceler.x<-0.2f)
	{
		containShip.image=shipLeft;
		containShip.frame=CGRectMake(containShip.frame.origin.x-3,containShip.frame.origin.y,containShip.frame.size.width, containShip.frame.size.height);
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------- BASIC --------------*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)jumpKirby
{
	/*------KIRBY-------*/
	[NSTimer scheduledTimerWithTimeInterval:0.02f target:self selector:@selector(behaviourKirby) userInfo:nil repeats:YES];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------- BEHAVIOURS --------------*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)behaviourKirby
{

	
		//définition de la vitesse de kirby
		vitesse = 20 * (containShip.frame.origin.y/300);
		
		//definition d'une vitesse minimale
		if(vitesse < 1){vitesse = 4;}
		
		/*------ Changement d'état ------*/
		if(containShip.frame.origin.y < 33)
		{
			containShip.image=shipFall;
			upDown = FALSE;
		}
		
		if(containShip.frame.origin.y > 375)
		{
			containShip.image=shipJump;
			upDown = TRUE; 
		}
		/*------ Changement d'état ------*/
		
		//Execution du saut
		if(upDown == TRUE)
		{
			containShip.frame=CGRectMake(containShip.frame.origin.x,containShip.frame.origin.y-vitesse,containShip.frame.size.width, containShip.frame.size.height);
		}
		else
		{
			containShip.frame=CGRectMake(containShip.frame.origin.x,containShip.frame.origin.y+vitesse,containShip.frame.size.width, containShip.frame.size.height);
		}
	
	/*------------- DECOR --------------*/
	
	if(containShip.frame.origin.y < 70)
	{
		bgMovementUp = 450/containShip.frame.origin.y;
		background.frame=CGRectMake(0,bgMovementUp,background.frame.size.width, background.frame.size.height);
		upBg.frame=CGRectMake(0,-940,upBg.frame.size.width,upBg.frame.size.height);
	}
	else if(containShip.frame.origin.y > 50)
	{
		bgMovementDown = 50/containShip.frame.origin.y;
		background.frame=CGRectMake(0,bgMovementDown,background.frame.size.width, background.frame.size.height);
		upBg.frame=CGRectMake(0,-960,upBg.frame.size.width,upBg.frame.size.height);
	}
	
	/*------------- DECOR --------------*/
	
	
	/*------------- COLLISIONS --------------*/
	
	//testApple
	hitTestP=CGRectIntersectsRect(containShip.frame, containApple.frame);
	
	
	if(hitTestP == TRUE)
	{
		containApple.alpha = 0; 
		if(hitStatusP == TRUE)
		{
			score=score+20; 
			hitStatusP = FALSE;
		}
	}
	
	//testApple2
	hitTestP=CGRectIntersectsRect(containShip.frame, containApple2.frame);
	
	
	if(hitTestP == TRUE)
	{
		containApple2.alpha = 0; 
		if(hitStatusP == TRUE)
		{
			score=score+20; 
			hitStatusP = FALSE;
		}
	}
	
	//testApple3
	hitTestP=CGRectIntersectsRect(containShip.frame, containApple3.frame);
	
	
	if(hitTestP == TRUE)
	{
		containApple3.alpha = 0; 
		if(hitStatusP == TRUE)
		{
			score=score+20; 
			hitStatusP = FALSE;
		}
	}
		
	
	//testBombe
	hitTestB=CGRectIntersectsRect(containShip.frame, containBombe.frame);
	
	if(hitTestB == TRUE)
	{
		containBombe.alpha = 0;
		if(hitStatusB == TRUE)
		{
			score=score-100; 
			hitStatusB = FALSE;
		}
	}
	
	
	/*---- SCORE ----*/
	showScore.text = [NSString stringWithFormat:@"%d", score];
	
	
	/*------------- COLLISIONS --------------*/

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------- BEHAVIOURS --------------*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)behaviours
{
	//

	/*------ Déplacement de la cerise ------*/
	if (containApple.frame.origin.x < 270) 
	{
		if (containApple.frame.origin.y < 500) 
		{
			containApple.frame=CGRectMake(containApple.frame.origin.x,containApple.frame.origin.y+5,containApple.frame.size.width,containApple.frame.size.height);
		}
		else 
		{
			containApple.frame=CGRectMake((arc4random()%200)+5,-200,containApple.frame.size.width,containApple.frame.size.height);
			containApple.alpha = 1;
			hitStatusP = TRUE;
		}
	}
	
	if (containApple2.frame.origin.x < 270) 
	{
		if (containApple2.frame.origin.y < 500) 
		{
			containApple2.frame=CGRectMake(containApple2.frame.origin.x,containApple2.frame.origin.y+4,containApple2.frame.size.width,containApple2.frame.size.height);
		}
		else 
		{
			containApple2.frame=CGRectMake((arc4random()%230)+5,-200,containApple2.frame.size.width,containApple2.frame.size.height);
			containApple2.alpha = 1;
			hitStatusP = TRUE;
		}
	}
	
	if (containApple3.frame.origin.x < 270) 
	{
		if (containApple3.frame.origin.y < 500) 
		{
			containApple3.frame=CGRectMake(containApple3.frame.origin.x,containApple3.frame.origin.y+3,containApple3.frame.size.width,containApple3.frame.size.height);
		}
		else 
		{
			containApple3.frame=CGRectMake((arc4random()%270)+5,-200,containApple3.frame.size.width,containApple3.frame.size.height);
			containApple3.alpha = 1;
			hitStatusP = TRUE;
		}
	}
	/*------ Déplacement de la cerise ------*/
	
	/*------ Déplacement de la bombe ------*/
	if (containBombe.frame.origin.x < 270) 
	{
		if (containBombe.frame.origin.y < 500) 
		{
			containBombe.frame=CGRectMake(containBombe.frame.origin.x,containBombe.frame.origin.y+3,containBombe.frame.size.width,containBombe.frame.size.height);
		}
		else 
		{
			containBombe.frame=CGRectMake((arc4random()%290)+5,-200,containBombe.frame.size.width,containBombe.frame.size.height);
			containBombe.alpha = 1;
			hitStatusB = TRUE;
		}
	}
	/*------ Déplacement de la bombe ------*/
}

/*------------- BEHAVIOURS --------------*/





/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
