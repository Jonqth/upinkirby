//
//  projetAppDelegate.h
//  projet
//
//  Created by etudiant on 13/01/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class projetViewController;

@interface projetAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    projetViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet projetViewController *viewController;

@end

