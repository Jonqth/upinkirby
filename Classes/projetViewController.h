//
//  projetViewController.h
//  projet
//
//  Created by etudiant on 13/01/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface projetViewController : UIViewController < UIAccelerometerDelegate >
{
	
	/*-------- IMAGES ----------*/
	IBOutlet UIImageView *background;
	IBOutlet UIImageView *upBg;
	IBOutlet UIImageView *containShip;
	IBOutlet UIImageView *containApple;
	IBOutlet UIImageView *containApple2;
	IBOutlet UIImageView *containApple3;
	IBOutlet UIImageView *containBombe;
	IBOutlet UIImageView *menu;
	IBOutlet UIImage *up;
	IBOutlet UIImage *ship;
	IBOutlet UIImage *shipJump;
	IBOutlet UIImage *shipFall;
	IBOutlet UIImage *shipLeft;
	IBOutlet UIImage *apple;
	IBOutlet UIImage *bombe;
	/*-------- IMAGES ----------*/
	
	/*-------- LABELS ----------*/
	IBOutlet UILabel *showScore;
	/*-------- LABELS ----------*/
	
	/*-------- LABELS ----------*/
	IBOutlet UIWebView *rulesView;
	/*-------- LABELS ----------*/
	
	
}

- (IBAction)clicButton:(id)sender;
- (IBAction)clicButton2:(id)sender;
- (IBAction)clicButton3:(id)sender;


@property(nonatomic, retain) IBOutlet UILabel *showScore;
@property(nonatomic, retain) IBOutlet UIButton *playButton;
@property(nonatomic, retain) IBOutlet UIButton *rulesButton;
@property(nonatomic, retain) IBOutlet UIButton *backButton;
@property(nonatomic, retain) IBOutlet UIWebView *rulesView;


@end

